/*
 * AVRAssembler1.asm
 *
 *  Created: 04.08.2015 21:27:47
 *   Author: Alexander Shaburov
 */ 

.include "m8def.inc"

; �������=============================

; ����� ��������======================

; RAM=================================
.dseg

; FLASH================================
.cseg 
	; ������---------------------------
	; ��������� ������ ��������� 
	.def temp	= r16
	.def temp1	= r17
	.def temp2	= r18
	.def temp3	= r19
	.def number = r20
	.def digit = r21
	; �������		  0b75643210
	.equ ddr_b		= 0b00111100
	.equ port_b		= $00
	.equ ddr_d		= $00
	.equ port_d		= 0b00011111
	;spi ������� spcr
	.equ spi_reg	= $73
	;timer 0 init
	.equ tim0_tccr0 = $02
	.equ tim0_tcnt  = $60
	.equ tim0_timsk = $01 
	; ���� �������------------------------------
	.org $00
	; ����� �������� ���������------------------
	rjmp Start
	; ������� ����������------------------------	
	.org $01 ; External Interrupt Request 0
		reti
	.org $02 ; External Interrupt Request 1
		reti
	.org $03 ; Timer/Counter2 Compare Match
		reti
	.org $04 ; Timer/Counter2 Overflow
		reti
	.org $05 ; Timer/Counter1 Capture Event
		reti
	.org $06 ; Timer/Counter1 Compare Match A
		reti 
	.org $07 ; Timer/Counter1 Compare Match B
		reti
	.org $08 ; Timer/Counter1 Overflow
		reti
	.org $09 ; Timer/Counter0 Overflow
		rjmp ovf_tim0
	.org $0a ; Serial Transfer Complete
		reti
	.org $0b ; USART, Rx Complete
		reti
	.org $0c ; USART Data Register Empty
		reti
	.org $0d ; USART, Tx Complete
		reti
	.org $0e ; ADC Conversion Complete
		reti
	.org $0f ; EEPROM Ready
		reti
	.org $10 ; Analog Comparator
		reti
	.org $11 ; 2-wire Serial Interface
		reti
	.org $12 ; Store Program Memory Ready
		reti
	.org INT_VECTORS_SIZE ; ����� ������� ����������
	; ����������------------------------------
ovf_tim0:	
	ldi zl, low(2*sym_number)
	ldi zh, high(2*sym_number)
	add zl, number
	lpm  ; ������� � �0 ����� �� �������

	ldi digit, 0
	rcall Spi_send_byte_595	
	reti
;***********************************************************
;************** �������������*******************************
;***********************************************************
Start:
	; ���� -----------------
	ldi temp, high(RAMEND)
	out SPH, temp
	ldi temp, low(RAMEND)
	out SPL, temp
	; ������ ���������----------------
	;�����
	ldi temp, ddr_b
	out DDRB, temp
	ldi temp, port_b
	out PORTB, temp
	ldi temp, ddr_d
	out DDRD, temp
	ldi temp, port_d
	out PORTD, temp
	;spi
	ldi temp, spi_reg
	out SPCR, temp
	;timer_0
	ldi temp, tim0_tccr0 
	out TCCR0, temp
	ldi temp, tim0_tcnt
	out TCNT0, temp
	ldi temp, tim0_timsk	
	out TIMSK, temp
	sei
;***********************************************************
;***************����� ���������*****************************
;***********************************************************
MainLoop:
	; ����
menu:	
	nop
	nop
	nop
	nop
	sbic PIND, 0
	rjmp start_rele
	rjmp menu
; ������ ����
start_rele:
	nop
	nop
	nop
	nop
	sbic PIND, 0
	rjmp menu
	rjmp start_rele
	rjmp MainLoop	

;***********************************************************
;**************������������*********************************
;***********************************************************
; �������--------------------------------------
S_DivByteByte:
	;temp - �������, temp1 - ��������
	;��������� ������� � temp, ������� � temp3
	ldi xl, 9
	clr temp3
	clc
S_Div_Byte_Byte_Loop:
	rol temp3
	sub temp3, temp1
	brcc S_Div_ByteByte_1
	add temp3, temp1
S_Div_ByteByte_1:
	rol temp
	dec xl
	brne S_Div_Byte_Byte_Loop
	com temp
	ret
; ���������� ����� �� 0 �� 99 �� �������----------
;temp1 - �����, temp1 - �������, temp2 - �������
decomp_7_segm:
	clr temp1
	clr temp2
decomp_10:
	cpi temp, $a
	brlo exit
	subi temp, $a
	inc temp1
	rjmp decomp_10
	mov temp2, temp
exit: ret	
; spi send data-----------------------------------
Spi_send_byte_595:
	cbi PORTB, 2
	out SPDR, digit
	rcall Wait_transmit
	out SPDR, r0
	rcall Wait_transmit
	sbi PORTB, 2
	ret
; �������� ���������� �������� 
Wait_transmit:
	sbis SPSR, SPIF
	rjmp Wait_transmit	
	ret
		
; ������� ��� 7 �����������
sym_number:
	.db $03, $9f ; 0, 1
	.db $25, $0d ; 2, 3
	.db $99, $49 ; 4, 5
	.db $81, $3f ; 6, 7
	.db $01, $09 ; 8, 9
; �������
sym_digit:
	.db $01, $02 ; 4, 3
	.db $04, $08 ; 2, 1

; EEPROM===============================
.eseg